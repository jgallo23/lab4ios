//
//  editFlashcardControllerViewController.swift
//  CS422L
//
//  Created by Jason Gallo on 2/15/22.
//

import UIKit

class editFlashcardControllerViewController: UIViewController {
    
    @IBOutlet var alertView: UIView!
    @IBOutlet var termText: UITextField!
    @IBOutlet var definitonText: UITextField!
    var parentView: FlashCardSetDetailViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termText.text = parentView.cards[parentView.selectedDialog].term
        definitonText.text = parentView.cards[parentView.selectedDialog].definition
    }
    
    @IBAction func done(_ sender: Any){
        parentView.cards[parentView.selectedDialog].term = termText.text ?? ""
        parentView.cards[parentView.selectedDialog].definition = definitonText.text ?? ""
        parentView.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func delet(_ sender: Any){
        parentView.cards.remove(at: parentView.selectedDialog)
        parentView.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }

}
