//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Jason Gallo on 2/15/22.
//

import UIKit

class StudySetViewController: UIViewController {
    
    var parentView : FlashCardSetDetailViewController!
    var counter: Int = 0
    var missing: Int = 0
    var correct: Int = 0
    var tableCards: [Flashcard] = [Flashcard]()
    
    
    @IBOutlet weak var showDef: UIButton!
    @IBOutlet weak var correctButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var missedButton: UIButton!
    @IBOutlet weak var cardCount: UITextField!
    @IBOutlet weak var missedCount: UITextField!
    @IBOutlet weak var correctCount: UITextField!
    @IBOutlet weak var termText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableCards = parentView.cards
        cardCount.text = "Card Left: " + String(tableCards.count)
        termText.text = tableCards[counter].term
        missedCount.text = "Missed: " + String(missing)
        correctCount.text = "Correct: " + String(correct)
    }
    
    
    
    func reloading(){
        if (tableCards.count == counter){
            counter = 0
        }
        if ( tableCards.isEmpty){
           finished()
            }
        else{
            cardCount.text = "Card Left: " + String(tableCards.count)
            termText.text = tableCards[counter].term
            missedCount.text = "Missed: " + String(missing)
            correctCount.text = "Correct: " + String(correct)
        }
    }
    
    func finished(){
        let winAlert = UIAlertController(title: "Done", message: "You got all of them correct!", preferredStyle: .alert)
        winAlert.addAction(UIAlertAction(title: "Go Back", style: .default, handler: {
            action in self.performSegue(withIdentifier: "goingBack", sender: nil)
        }))
        self.present(winAlert, animated: true)
    }
    
    
    
    @IBAction func definitionView(_ sender: Any){
        let defAlert = UIAlertController(title: "Definition", message: tableCards[counter].definition, preferredStyle: .alert)
        defAlert.addAction(UIAlertAction(title: "Got It", style: .cancel, handler: nil))
        self.present(defAlert, animated: true)
    }
    
    @IBAction func wrong(_ sender: Any){
        missing = missing + 1
        counter = counter + 1
        reloading()
    }
    
    @IBAction func skipping(_ sender: Any){
        counter = counter + 1
        reloading()
    }
    
    @IBAction func right(_ sender: Any){
        correct = correct + 1
        tableCards.remove(at: counter)
        reloading()
    }
}
