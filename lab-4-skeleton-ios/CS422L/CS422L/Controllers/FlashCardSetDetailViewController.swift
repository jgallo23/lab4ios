//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var cards: [Flashcard] = [Flashcard]()
    var selectedDialog : Int = 0
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    @IBAction func studying(_ sender: Any){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "StudySetViewController") as! StudySetViewController
        vc.parentView = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true)
        self.performSegue(withIdentifier: "goStudy", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Flashcard clicked
        clickedDialog(indexPath: indexPath)
    }
    
    func clickedDialog(indexPath: IndexPath){
        let dialog = UIAlertController(title: "\(cards[indexPath.row].term)", message: "\(cards[indexPath.row].definition)", preferredStyle: .alert)
        selectedDialog = indexPath.row
        dialog.addAction(UIAlertAction(title: "Edit", style: .destructive, handler: {_ in
            dialog.dismiss(animated: true, completion: {})
            self.editDialog()
        }))
        self.present(dialog, animated: true)
    }
    
    func editDialog(){
        let storyB = UIStoryboard(name: "Main", bundle: nil)
        let alertView = storyB.instantiateViewController(withIdentifier: "editFlashcardViewController") as! editFlashcardControllerViewController
        alertView.parentView = self
        alertView.modalPresentationStyle = .overCurrentContext
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
}
